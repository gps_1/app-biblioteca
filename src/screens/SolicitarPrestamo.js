import React, { useState, useEffect } from 'react';
import { Alert, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
// Import para los input de fecha
import DatePicker from 'react-native-datepicker';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function SolicitarPrestamo(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar la fecha para recoger el libro prestado*/
    const [dateR, setDateR] = useState(new Date());

    /*Estado para guardar la fecha para devolver el libro prestado*/
    const [dateD, setDateD] = useState(new Date());

    /*Estado para guardar status de prestamo*/
    const [status, setStatus] = useState(false);

    /*Estado para guardar el id de libro*/
    const [idLibro, setIdLibro] = useState(props.route.params.id_libro);

    /*Estado para guardar titulo*/
    const [titulo, setTitulo] = useState(props.route.params.nom_libro);

    /*Estado para guardar el autor*/
    const [autor, setAutor] = useState(props.route.params.autor_libro);

    /*Estado para guardar imágen*/
    const [imagen, setImagen] = useState(props.route.params.img_libro);

    /*Estado para guardar el id de usuario*/
    const [idUser, setUserId] = useState(props.route.params.id_usuario);

    /*Estado para guardar el nombre de usuario*/
    const [nombre, setNombre] = useState(props.route.params.nom_usuario);

    /*Estado para guardar el apellido1 de usuario*/
    const [ap1, setAp1] = useState(props.route.params.ap1_usuario);

    /*Estado para guardar el apellido 2 de usuario*/
    const [ap2, setAp2] = useState(props.route.params.ap2_usuario);

    return cargando ? <Loader
        mensaje='Cargando, por favor espere...' /> : (
        <ScrollView style={estilo.back}>
            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 50, color: '#fff' }}>PRESTAMO DE LIBRO</Text>
            </View>

            <View style={{ alignSelf: 'center', marginVertical: 50 }}>
                <Text style={{ alignSelf: 'center', marginVertical: 5, fontWeight: 'bold', fontSize: 20 }}>Fecha para recoger libro</Text>
                <DatePicker
                    style={{ width: 200 }}
                    mode="date"
                    format="YYYY-MM-DD"
                    date={dateR}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { setDateR(date) }}
                />
            </View>

            <View style={{ alignSelf: 'center', marginVertical: 50 }}>
                <Text style={{ alignSelf: 'center', marginVertical: 5, fontWeight: 'bold', fontSize: 20 }}>Fecha para devolver libro</Text>
                <DatePicker
                    style={{ width: 200 }}
                    mode="date"
                    format="YYYY-MM-DD"
                    minDate={dateR}
                    date={dateD}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { setDateD(date) }}
                />
            </View>

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /* Validación de campos completos */
                    if (dateR === '' || dateD === '') {
                        Alert.alert(
                            'Campos vacios',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);
                        // Aquí van los datos como están en la BD | Los estados que fueron obtenidos de los input
                        const params = JSON.stringify({
                            "Id_usuario": idUser,
                            "Id_libro": idLibro,
                            "Fecha_prestamo": dateR,
                            "Fecha_devolucion": dateD,
                            "Estado_prestamo": status,
                            "Nombre_usuario": nombre,
                            "Apellido1_usuario": ap1,
                            "Apellido2_usuario": ap2,
                            "Titulo_libro": titulo,
                            "Autor_libro": autor,
                            "Imagen_libro": imagen
                        });

                        // Función donde se realiza la petición al servidor para enviar los datos
                        Axios({
                            method: 'post',
                            url: `${baseUrl}/Prestamos/AddPrestamo`,
                            data: params,
                            headers: { 'Content-Type': 'application/json' }
                        }).then((response) => {
                            console.log(response);
                            setCargando(false);
                            Alert.alert(
                                'Registro Exitoso',
                                'Prestamo en proceso de aceptación',
                                [
                                    {
                                        text: 'Continuar',
                                        onPress: () => props.navigation.navigate('ListadoLibros')
                                    }
                                ],
                                {
                                    cancelable: false
                                }
                            );
                        }).catch((error) => {
                            setCargando(false);
                            console.log(error);
                            Alert.alert(
                                'Error',
                                'No fue posible registrar el prestamo, intente nuevamente'
                            );
                        });

                    }
                }}
            >
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Pedir libro
                    </Text>
                </View>
            </TouchableOpacity>

        </ScrollView>
    );
}