import React, { useState, useEffect } from 'react';
import { Alert, FlatList, Image, Text, View, Button, TextInput } from 'react-native';
import { baseUrl, estilo } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function BuscarLinea() {

  /*Estado para mostrar y quitar el loader*/
  const [cargando, setCargando] = useState(false);

  // Estado para guardar el arreglo de objetos libro
  const [listaLibros, setListaLibros] = useState([]);
  const [busqueda, setBusqueda] = useState("");

  // Función donde se realiza la petición al servidor para traer los libros
  useEffect(() => {
    setCargando(true);
    async function getLibros() {
      await Axios({
        method: "get",
        url: `https://api.itbook.store/1.0/search/new`
      }).then((response) => {
        setCargando(false);
        setListaLibros(response.data.books);
        //console.log(`Hola-- ${response.data.books}`);
      }).catch(() => {
        setCargando(false);
        Alert.alert(
          'Error',
          'No fue posible ver el listado de libros, intente nuevamente'
        );
      });
    }
    getLibros();

  }, []);

  return cargando ? <Loader
    mensaje='Espere...' /> : (
    <View style={{ backgroundColor: "#FFF", flexGrow: 1 }}>
      {/* Este Flatlist es plano, solo se visualizan los libros */}
      <TextInput
        placeholder='Escriba una búsqueda'
        keyboardType='default'
        style={{
          borderColor: '#000',
          borderWidth: 1,
          margin: 5,
          marginHorizontal: 15,
          padding: 10,
          borderRadius: 5,
          fontSize: 20,
          textAlign: 'center',
          marginVertical: 15
        }}
        //style={{[estilo.input, { flex: 0.8 }]}}
        onChange={val => setBusqueda(val.nativeEvent.text)}
        maxLength={50}
      />
      <Button style={{ flex: 0.2 }} color='green' title='Buscar' onPress={() => {
        if (busqueda === '') {
          Alert.alert(
            'Búsqueda vacía',
            `Por favor, ingrese una búsqueda`,
            [
              {
                text: 'Aceptar',
              }
            ],
            { cancelable: false }

          );
        } else {
          setCargando(true);
          Axios({
            method: "get",
            url: `https://api.itbook.store/1.0/search/${busqueda}`
          }).then((response) => {
            setCargando(false);
            if (response.data.books === []) {
              Alert.alert(
                'Ups',
                'Su búsqueda no arroja resultados, intente con otro término.'
              );
            } else {
              setListaLibros(response.data.books);
            }

          }).catch(() => {
            setCargando(false);
            Alert.alert(
              'Error',
              'No fue posible realizar la búsqueda, intente nuevamente'
            );
          });
        }
      }} />

      <FlatList
        data={listaLibros}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item, index) => {
          return (
            <View
              key={`Libro-${index}`}
              style={{
                marginVertical: 5,
                marginHorizontal: 5,
                marginBottom: 10,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 5,
                elevation: 4,
              }}
            >
              <Text key={`T-${index}`} style={{ marginVertical: 10, fontWeight: 'bold', alignSelf: 'center', fontSize: 20 }}>{item.item.title}</Text>
              <Image key={`I-${index}`} source={{ uri: item.item.image }}
                style={{ width: 200, height: 300, alignSelf: 'center', marginVertical: 15, resizeMode: "stretch" }} />
              <Text key={`T2-${index}`} style={{ marginVertical: 10, fontWeight: 'bold', alignSelf: 'center', fontSize: 20 }}>Precio: {item.item.price}</Text>
            </View>
          );
        }}
      />
    </View>
  );
}