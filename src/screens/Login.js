import React, { useState, useEffect } from 'react';
import { Alert, TextInput, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import { FontAwesome5 } from '@expo/vector-icons';
import md5 from 'react-native-md5';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function Login(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar el correo*/
    const [correo, setCorreo] = useState('');

    /*Estado para guardar la contraseña*/
    const [password, setPassword] = useState('');

    /*Estado para validar login*/
    const [usuario, setUsuario] = useState({
        email: '',
        password: '',
        tipo: 0
    });

    // Estado para guardar el arreglo de objetos usuario
    const [listaUsuarios, setListaUsuarios] = useState([]);

    // Función donde se realiza la petición al servidor para traer los usuarios
    useEffect(() => {
        async function getUsuarios() {
            await Axios({
                method: 'get',
                url: `${baseUrl}/Usuarios/GetUsuarios`
            }).then((response) => {
                setListaUsuarios(response.data);
            }).catch(() => {
                Alert.alert(
                    'Error',
                    'No fue posible inicir sesión, intente nuevamente'
                );
            });
        }
        getUsuarios();
    }, []);

    return (
        <ScrollView style={estilo.back}>

            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 60, color: '#fff' }}>INICIO DE SESIÓN</Text>
            </View>

            <FontAwesome5
                name='book'
                size={80} color='#4b0082'
                style={{ margin: 10, alignSelf: 'center' }} />

            {/* Inputs para ingresar datos del usuario */}
            <TextInput
                placeholder='Correo Electrónico'
                keyboardType='email-address'
                style={estilo.input}
                onChange={val => setCorreo(val.nativeEvent.text)}
                maxLength={100}
            />

            <TextInput
                placeholder='Contraseña'
                keyboardType='default'
                style={estilo.input}
                secureTextEntry={true}
                onChange={val => setPassword(val.nativeEvent.text)}
                maxLength={12}
            />

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /*  Se valida que no hayan campos vacios */
                    if (correo === '' || password === '') {
                        Alert.alert(
                            'Campos vacíos',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);
                        let existe = 0;
                        // Se recorre el arreglo de usuarios
                        listaUsuarios.map((us, index) => {
                            if (existe !== 1) {
                                //Validar si los datos de la nube son iguales a los ingresados por el usuario
                                if (us.email === correo && us.password === md5.hex_md5(password)) {
                                    setUsuario(
                                        {
                                            email: us.email,
                                            password: us.password,
                                            tipo: us.tipo_usurio
                                        }
                                    )
                                    /*Parametros que van a llegar hasta solicitar prestamo (pasaran entre screens)*/
                                    const id = us.idUsuario;
                                    const name = us.nombre;
                                    const ap1 = us.apellido1;
                                    const ap2 = us.apellido2;
                                    // Aquí redirecciona a su ventana principal a cada tipo de usuario
                                    switch (us.tipo_usuario) {
                                        case 1:
                                            existe = 1;
                                            props.navigation.replace('ListadoLibros', { id, name, ap1, ap2 });
                                            break;
                                        case 2:
                                            existe = 1;
                                            props.navigation.replace('PrincipalBibliotecario');
                                            break;
                                        case 3:
                                            existe = 1;
                                            props.navigation.replace('PrincipalAdmin');
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            // Validación para los datos ingresados que no existen en el arreglo
                            if (index + 1 === listaUsuarios.length && existe === 0) {
                                setCargando(false);
                                Alert.alert(
                                    'Error',
                                    `Correo electrónico o contraseña incorrecta`,
                                    [
                                        {
                                            text: 'Aceptar',
                                        }
                                    ],
                                    { cancelable: false }

                                );
                            }

                        })
                    }
                }}
            >
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Iniciar sesión
                    </Text>
                </View>
            </TouchableOpacity>


            <TouchableOpacity
                style={estilo.boton}
                onPress={() => props.navigation.navigate('PrincipalInvitado')}
            >
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Ingresar como invitado
                    </Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => props.navigation.navigate('Registro')}
            >
                <View>
                    <Text style={estilo.link}>
                        ¿No tienes una cuenta? Regístrate
                    </Text>
                </View>
            </TouchableOpacity>

        </ScrollView>
    );
}