import React from 'react';
import { Alert, Button, TextInput, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import { FontAwesome5 } from '@expo/vector-icons';
import Loader from '../libs/Loader';
//import Logout from './Logout';

export default function PrincipalAdmin(props) {

  return (
    <ScrollView style={estilo.back}>
      <TouchableOpacity onPress={() => props.navigation.navigate('ListaUsuarios')}
        style={{ backgroundColor: '#ffa833', margin: 60, padding: 25, borderRadius: 5, fontSize: 20, marginVertical: 110 }}>
        <View>
          <Text style={{ fontSize: 20, textAlign: 'center', color: '#000' }}>
            Usuarios
          </Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.navigation.navigate('ListaLibrosAdmin')}
        style={{ backgroundColor: '#ffa833', margin: 60, padding: 25, borderRadius: 5, fontSize: 20, marginVertical: 20 }}>
        <View>
          <Text style={{ fontSize: 20, textAlign: 'center', color: '#000' }}>
            Libros
          </Text>
        </View>
      </TouchableOpacity>

      <View style={{ padding: 120 }}>
        <View style={{ alignSelf: 'center' }}>
          <FontAwesome5
            name='power-off'
            size={23}
            color='tomato'
          />
        </View>
        <Button
          color='tomato'
          title='CERRAR SESIÓN'
          onPress={() => {
            Alert.alert('Alerta', `¿Estas seguro de salir?`,
              [
                {
                  text: 'Cancelar',
                }
                ,
                {
                  text: 'Aceptar',
                  onPress: () => {
                    props.navigation.replace('Login');
                  }
                }
              ],
              {
                cancelable: false
              }
            );
          }}
        />
      </View>

    </ScrollView>
  );
}