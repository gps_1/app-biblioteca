import React, { useState } from 'react';
import { Alert, Button, Image, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import { Picker } from "@react-native-picker/picker";
import Loader from '../libs/Loader';
import Axios from 'axios';
/* imports para usar al llamar los servicios
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import md5 from 'react-native-md5';
import * as SecureStore from 'expo-secure-store';
*/

export default function AgregarLibro(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar el nombre*/
    const [nombre, setNombre] = useState('');

    /*Estado para guardar la imagen*/
    //const [imagen, setImagen] = useState('');

    /*Estado para guardar el nombre de autor*/
    const [autor, setAutor] = useState('');

    /*Estado para guardar el status*/
    const [status, setStatus] = useState(true);

    return cargando ? <Loader
        mensaje='Cargando, por favor espere...' /> : (
        <ScrollView style={estilo.back}>
            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 50, color: '#fff' }}>AGREGAR LIBRO 1/2</Text>
            </View>

            {/* Inputs para ingresar datos del libro */}
            <TextInput
                placeholder='Nombre'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setNombre(val.nativeEvent.text)}
                maxLength={100}
            />
            <TextInput
                placeholder='Autor'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setAutor(val.nativeEvent.text)}
                maxLength={100}
            />
            <View style={{ paddingHorizontal: 99 }}>
                <Text style={{ alignSelf: 'center', fontWeight: 'bold', fontSize: 18 }}>Estatus de libro</Text>
                <Picker
                    selectedValue={status}
                    onValueChange={(item, index) => setStatus(item)}>
                    <Picker.Item label="--Elija una opción--" />
                    <Picker.Item label="Disponible" value={true} />
                    <Picker.Item label="No disponible" value={false} />
                </Picker>
            </View>

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /* Validación de campos completos */
                    if (nombre === '' || autor === '' || status === '') {
                        Alert.alert(
                            'Campos vacios',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);

                        Alert.alert('Siguiente', `Agregue una imagen`,
                            [
                                {
                                    text: 'Cancelar',
                                }
                                ,
                                {
                                    text: 'Aceptar',
                                    onPress: () => {
                                        props.navigation.navigate('SubirImagen', {
                                            titulo: nombre,
                                            autor: autor,
                                            estado: status
                                        });
                                    }
                                }
                            ],
                            {
                                cancelable: false
                            }
                        );
                    }
                }}>
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Registrar libro
                    </Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
    );
}